package com.example.myweapondbapp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TableWeapons
{
    private DBHelper dbHelper;
    private final String TABLE_NAME = "weapons";

    public TableWeapons(Context context)
    {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Weapon> GetAll()
    {
        ArrayList<Weapon> weapons = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(TABLE_NAME,null, null, null, null, null, null);

        //Cursor cursor = db.rawQuery("SELECT * FROM weapons", null);

        if(cursor.moveToFirst()==true)
        {
            do
            {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String material = cursor.getString(2);
                String description = cursor.getString(3);

                weapons.add(new Weapon(id,name,material,description));

            }while (cursor.moveToNext()==true);
        }

        cursor.close();

        dbHelper.close();

        return weapons;
    }

    public void DeleteAll()
    {
        //db.execSQL("DELETE FROM weapons");
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(TABLE_NAME, null, null);

        dbHelper.close();
    }

    public void DeleteById(int id)
    {
        //db.execSQL("DELETE FROM weapons WHERE id="+id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(TABLE_NAME, "id ="+id, null);

        dbHelper.close();
    }

    public void AddNew(Weapon weapon)
    {
        //db.execSQL("INSERT INTO weapons (name, material, description) \n" +
        //        "VALUES ('"+weapon.getName()+"','"+weapon.getMaterial()+"','"+weapon.getDescription()+"')");
        ContentValues cv = new ContentValues();

        cv.put("name",weapon.getName());
        cv.put("material",weapon.getMaterial());
        cv.put("description",weapon.getDescription());

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.insert(TABLE_NAME, null, cv);

        dbHelper.close();
    }

    public void UpdateById(Weapon weapon)
    {
        //db.execSQL("UPDATE weapons SET name='"+weapon.getName()+"',material='"+weapon.getMaterial()+"',description='"+weapon.getDescription()+"' WHERE id ="+weapon.getId());
        ContentValues cv = new ContentValues();

        cv.put("name",weapon.getName());
        cv.put("material",weapon.getMaterial());
        cv.put("description",weapon.getDescription());

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.update(TABLE_NAME, cv, "id="+weapon.getId(), null);

        dbHelper.close();
    }
}
