package com.example.myweapondbapp2;

public class Weapon
{
    private int Id;
    private String Name;
    private String Material;
    private String Description;

    public Weapon (int Id, String Name, String Material, String  Description)
    {
        this.Id = Id;
        this.Name = Name;
        this.Material = Material;
        this.Description = Description;
    }

    public int getId()
    {
        return Id;
    }

    public String getName()
    {
        return Name;
    }

    public String getMaterial()
    {
        return Material;
    }

    public String getDescription()
    {
        return Description;
    }
}
