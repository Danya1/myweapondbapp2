package com.example.myweapondbapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button buttonAddWeapon, buttonEditWeapon, buttonClearAllWeapons;
    EditText editTextName, editTextMaterial, editTextDescription;
    ListView listViewWeapons;

    TableWeapons tableWeapons;
    Weapon selectedWeapon;

    private void UpdateListViewWeapons()
    {
        WeaponAdapter weaponAdapter = new WeaponAdapter(getApplicationContext(), tableWeapons.GetAll());

        listViewWeapons.setAdapter(weaponAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        editTextMaterial = findViewById(R.id.editTextMaterial);
        editTextDescription = findViewById(R.id.editTextDescription);

        buttonAddWeapon = findViewById(R.id.buttonAddWeapon);
        buttonAddWeapon.setOnClickListener(buttonAddWeaponClickListener);

        buttonClearAllWeapons = findViewById(R.id.buttonClearAllWeapons);
        buttonClearAllWeapons.setOnClickListener(buttonClearAllWeaponsClickListener);

        buttonEditWeapon = findViewById(R.id.buttonEditWeapon);
        buttonEditWeapon.setVisibility(View.GONE);
        buttonEditWeapon.setOnClickListener(buttonEditWeaponClickListener);

        listViewWeapons = findViewById(R.id.listViewWeapons);

        //SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app.db",MODE_PRIVATE, null);
        tableWeapons = new TableWeapons(getApplicationContext());

        UpdateListViewWeapons();

        registerForContextMenu(listViewWeapons);
        selectedWeapon=null;
    }

    View.OnClickListener buttonAddWeaponClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editTextName.getText().toString();
            String material = editTextMaterial.getText().toString();
            String description = editTextDescription.getText().toString();

            if (name.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Name!", Toast.LENGTH_LONG).show();
                return;
            }
            if (material.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Material!", Toast.LENGTH_LONG).show();
                return;
            }

            tableWeapons.AddNew(new Weapon(0,name,material,description));
            UpdateListViewWeapons();

            editTextName.getText().clear();
            editTextMaterial.getText().clear();
            editTextDescription.getText().clear();

            Toast.makeText(getApplicationContext(), "Weapon added", Toast.LENGTH_LONG).show();
        }
    };

    View.OnClickListener buttonEditWeaponClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editTextName.getText().toString();
            String material = editTextMaterial.getText().toString();
            String description = editTextDescription.getText().toString();

            if (name.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Name!", Toast.LENGTH_LONG).show();
                return;
            }
            if (material.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Material!", Toast.LENGTH_LONG).show();
                return;
            }

            Weapon weapon = new Weapon(selectedWeapon.getId(),name,material,description);
            tableWeapons.UpdateById(weapon);

            UpdateListViewWeapons();

            editTextName.getText().clear();
            editTextMaterial.getText().clear();
            editTextDescription.getText().clear();

            Toast.makeText(getApplicationContext(), "Weapon edited", Toast.LENGTH_LONG).show();
            buttonEditWeapon.setVisibility(View.GONE);
            buttonAddWeapon.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener buttonClearAllWeaponsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tableWeapons.DeleteAll();

            Toast.makeText(getApplicationContext(), "Weapons deleted", Toast.LENGTH_LONG).show();

            UpdateListViewWeapons();
        }
    };

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(0,0,0,"Edit weapon");
        menu.add(0,1,0,"Delete weapon");

        int clickPosition = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
        selectedWeapon = (Weapon) ((ListView)v).getItemAtPosition(clickPosition);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item)
        {
        switch (item.getItemId())
        {
            case 0:
                buttonEditWeapon.setVisibility(View.VISIBLE);
                buttonAddWeapon.setVisibility(View.GONE);

                editTextName.setText(selectedWeapon.getName());
                editTextMaterial.setText(selectedWeapon.getMaterial());
                editTextDescription.setText(selectedWeapon.getDescription());
                break;
            case 1:
                tableWeapons.DeleteById(selectedWeapon.getId());

                UpdateListViewWeapons();
                Toast.makeText(getApplicationContext(), "Weapon deleted", Toast.LENGTH_LONG).show();
                break;
        }

        return true;
    }
}
